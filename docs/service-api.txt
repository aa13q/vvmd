Service hierarchy
=================

Service		org.kop316.vvm
Interface	org.kop316.vvm.Service
Object path	[variable prefix]/{service0,service1,...}

Methods		array{object,dict} GetMessages()

			Get an array of message objects and properties
			that represents the currently received and sent
			messages.

			This method call should only be used once when an
			service becomes available.  Further message additions
			and removal shall be monitored via MessageAdded and
			MessageRemoved signals.

			Possible Errors: [service].Error.InvalidArguments

Signals		MessageAdded(object path, dict properties)

			Signal that is sent when a new message is added. It
			contains the object path of new message, its
			properties.

		MessageRemoved(object path)

			Signal that is sent when a message has been removed.
			The object path is no longer accessible after this
			signal and only emitted for reference.
