/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <gio/gio.h>

#define VVM_SERVICE	"org.kop316.vvm"
#define VVM_PATH	"/org/kop316/vvm"

#define VVM_MANAGER_INTERFACE	VVM_SERVICE ".Manager"
#define VVM_SERVICE_INTERFACE	VVM_SERVICE ".Service"
#define VVM_MESSAGE_INTERFACE	VVM_SERVICE ".Message"

#define VVM_ERROR_INTERFACE	VVM_SERVICE ".Error"

GDBusConnection *vvm_dbus_get_connection(void);
GDBusNodeInfo   *vvm_dbus_get_introspection_data(void);
void __vvm_dbus_set_connection(GDBusConnection *conn);
void __vvm_dbus_set_introspection_data(void);
void __vvm_dbus_unref_introspection_data(void);

/* ---------------------------------------------------------------------------------------------------- */

/*
 * GIO Dbus Server modified from here:
 * https://gitlab.gnome.org/GNOME/glib/-/blob/master/gio/tests/gdbus-example-server.c
 */

/* Introspection data for the service we are exporting */
static const gchar introspection_xml[] =
  "<node>"
  "  <interface name='org.kop316.vvm.Service'>"
  "    <method name='GetMessages'>"
  "      <arg type='a(oa{sv})' name='messages_with_properties' direction='out'/>"
  "    </method>"
  "    <method name='GetProperties'>"
  "      <arg type='a{sv}' name='properties' direction='out'/>"
  "    </method>"
  "    <signal name='MessageAdded'>"
  "      <arg type='o' name='path'/>"
  "      <arg type='a{sv}' name='properties'/>"
  "    </signal>"
  "    <signal name='MessageRemoved'>"
  "      <arg type='o' name='path'/>"
  "    </signal>"
  "  </interface>"
  "  <interface name='org.kop316.vvm.Manager'>"
  "    <method name='GetServices'>"
  "      <arg type='a(oa{sv})' name='services_with_properties' direction='out'/>"
  "    </method>"
  "    <signal name='ServiceAdded'>"
  "      <arg type='o' name='path'/>"
  "      <arg type='a{sv}' name='properties'/>"
  "    </signal>"
  "    <signal name='ServiceRemoved'>"
  "      <arg type='o' name='path'/>"
  "    </signal>"
  "  </interface>"
  "  <interface name='org.kop316.vvm.Message'>"
  "    <method name='Delete'/>"
  "    <method name='MarkRead'/>"
  "    <signal name='PropertyChanged'>"
  "      <arg type='s' name='name'/>"
  "      <arg type='v' name='value'/>"
  "    </signal>"
  "  </interface>"
  "</node>";

/* ---------------------------------------------------------------------------------------------------- */
