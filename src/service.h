/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2010-2011, Intel Corporation
 *                2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

struct vvm_service;
struct vvm_message;

typedef void (*vvm_service_bearer_handler_func_t) (gboolean active,
							void *user_data);

struct vvm_service *vvm_service_create(void);
int vvm_service_register(struct vvm_service *service);
int vvm_service_unregister(struct vvm_service *service);

int vvm_service_set_identity(struct vvm_service *service,
					const char *identity);
GKeyFile *vvm_service_get_keyfile(struct vvm_service *service);

int vvm_service_set_country_code(struct vvm_service 	*service,
			 	 const char 		*imsi);

gchar *vvm_message_format_number_e164(const char *number,
				      const char *country_code,
				      gboolean return_original_number);

int vvm_service_set_own_number(struct vvm_service 	*service,
			       const char 		*own_number);

int vvm_service_sync_vvm_imap_server(struct vvm_service *service);

void vvm_service_set_hostname(struct vvm_service *service,
			     const char		*mailbox_hostname);

void vvm_service_set_port(struct vvm_service *service,
			 const char	    *mailbox_port);

void vvm_service_set_username(struct vvm_service *service,
			      const char	 *mailbox_username);

void vvm_service_set_password(struct vvm_service *service,
			      const char	 *mailbox_password);

void vvm_service_set_language(struct vvm_service *service,
			      const char	 *language);

void vvm_service_set_greeting_length(struct vvm_service *service,
				     const char		*greeting_length);

void vvm_service_set_voice_signature_length(struct vvm_service *service,
				       	    const char	       *voice_signature_length);

void vvm_service_set_tui_password_length(struct vvm_service *service,
				   	 const char	    *TUI_password_length);

void vvm_service_set_subscription_configuration(struct vvm_service *service,
						const char	   *configuration);

int vvm_service_new_vvm (struct vvm_service *service, char *sync_message, char *index, const char *mailbox_config);

void vvm_service_deactivate_vvm_imap_server(struct vvm_service *service);
