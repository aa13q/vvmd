/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2010-2011, Intel Corporation
 *                2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#define VVM_SHA1_UUID_LEN 20
#define VVM_META_UUID_SUFFIX ".status"
#define VVM_META_UUID_SUFFIX_LEN 7

#define VVM_META_EMAIL_SUFFIX ".mbox"

/***** SETTINGS CONFIGURATION ****/
#define STORAGE_FOLDER ".vvm"
#define SETTINGS_STORE "vvm"
#define SETTINGS_GROUP_SERVICE "Settings"

int vvm_store(const char *service_id,
	      const char *pdu,
	      const unsigned char *unsigned_pdu,
	      unsigned int len,
	      const char *filename,
	      const char *path);

int vvm_store_file(const char *service_id, const char *path, const char *uuid);
void vvm_store_remove(const char *service_id, const char *uuid);
void vvm_store_remove_attachments(const char *service_id, const char *uuid, const char *attachments);
char *vvm_store_get_path(const char *service_id, const char *uuid);
GKeyFile *vvm_store_meta_open(const char *service_id, const char *uuid);
void vvm_store_meta_close(const char *service_id, const char *uuid,
					GKeyFile *keyfile, gboolean save);

GKeyFile *vvm_settings_open(const char *service_id, const char *store);
void vvm_settings_close(const char *service_id, const char *store,
					GKeyFile *keyfile, gboolean save);
