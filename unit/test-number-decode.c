/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <glib.h>
#include <glib/gprintf.h>

#include "vvmutil.h"

static void test_number_decode(gconstpointer data)
{
	char *output;

	const char decode_t_mobile_to[] = "VOICE=+12065550110@domain.com";
	const char decode_t_mobile_from[] = "VOICE=+14325550110@tmo.com";

	const char decode_mint_mobile_to[] = "VOICE=4325550110@domain.com";
	const char decode_mint_mobile_from[] = "VOICE=2065550110@tmo.com";

	const char decode_att_usa_to[] = "\"4325550110\" <4325550110@crafpaalvml001.nsdeng.att.com>";
	const char decode_att_usa_from[] = "\"2065550110\" <2065550110@crafpaalvml001.nsdeng.att.com>";

	output = parse_email_address(decode_t_mobile_to);
	g_assert(g_strcmp0(output, "+12065550110") == 0);
	g_free(output);
	output = NULL;

	output = parse_email_address(decode_t_mobile_from);
	g_assert(g_strcmp0(output, "+14325550110") == 0);
	g_free(output);
	output = NULL;

	output = parse_email_address(decode_mint_mobile_to);
	g_assert(g_strcmp0(output, "4325550110") == 0);
	g_free(output);
	output = NULL;

	output = parse_email_address(decode_mint_mobile_from);
	g_assert(g_strcmp0(output, "2065550110") == 0);
	g_free(output);
	output = NULL;

	output = parse_email_address(decode_att_usa_to);
	g_assert(g_strcmp0(output, "4325550110") == 0);
	g_free(output);
	output = NULL;

	output = parse_email_address(decode_att_usa_from);
	g_assert(g_strcmp0(output, "2065550110") == 0);
	g_free(output);
	output = NULL;

}

int main(int argc, char **argv)
{
	g_test_init(&argc, &argv, NULL);

	g_test_add_data_func("/vvmutil/Number Decode Test",
				NULL, test_number_decode);


	return g_test_run();
}
